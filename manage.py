#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "JPLearn.settings")

    # if sys.argv[1] == 'test':
    if True:
        os.environ.setdefault(
            "NEO4J_REST_URL", 'http://neo4j:test@localhost:7474/db/data/')
        os.system('./test_database/bin/neo4j start')
    # else:
    #     os.environ.setdefault("NEO4J_REST_URL", 'http://neo4j:neo4j@localhost:7474/db/data/')
    #     os.system('./database/bin/neo4j start')

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

    if sys.argv[1] == 'test':
        os.environ.unsetenv("NEO4J_REST_URL")
        # os.system('./test_database/bin/neo4j stop')
