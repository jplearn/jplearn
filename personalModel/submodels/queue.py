from languageModel.models import Kanji, Word
from neomodel import (StructuredNode, StringProperty,
                      IntegerProperty, RelationshipTo, RelationshipFrom, ZeroOrOne, One)
import time


class Queue(StructuredNode):
    user = RelationshipFrom('User', 'Queue', cardinality=One)

    queueStart = RelationshipTo(
        'QueueToken', 'QueueStart', cardinality=ZeroOrOne)

    queueTokens = RelationshipTo('QueueToken', 'QueueToken')

    unlearnedKanji = RelationshipTo('UnlearnedKanji', 'UnlearnedKanji')

    queueEnd = RelationshipTo(
        'QueueToken', 'QueueEnd', cardinality=ZeroOrOne)

    def filterUnlearnedKanji(self, **kwargs):
        try:
            return self.queueTokens.search(**kwargs)
        except ValueError:

            cypher = "START a=node({self}) MATCH a-[:UnlearnedKanji]->(b)-[:Origin]->(c)"
            if (len(kwargs)):
                cypher += " WHERE "
            for key, value in kwargs.items():
                cypher += "c.neo_{0} = \'{1}\', ".format(key, value)

            cypher = cypher[0: -2]
            cypher += " RETURN b"

            results, columns = self.cypher(cypher, kwargs)
            return [PersonalizedWord.inflate(row[0]) for row in results]

    def push(self, queueToken):
        self.queueTokens.connect(queueToken)
        if len(self.queueEnd):
            self.queueEnd.get().setNextToken(queueToken)
            self.queueEnd.get().save()
            self.queueEnd.disconnect(self.queueEnd.get())
        else:
            self.queueStart.connect(queueToken)

        self.queueEnd.connect(queueToken)
        return self.save()

    def popBack(self):
        lastToken = self.queueEnd.get()
        if len(lastToken.previousToken):
            preLastToken = lastToken.previousToken.get()
            self.queueEnd.disconnect(lastToken)
            self.queueEnd.connect(preLastToken)
        lastToken.delete()
        return self.save()

    def front(self):
        return self.queueStart.get().origin.get()

    def back(self):
        return self.queueEnd.get().origin.get()

    def popFront(self):
        firstToken = self.queueStart.get()
        if len(firstToken.nextToken):
            secondToken = firstToken.nextToken.get()
            self.queueStart.disconnect(firstToken)
            self.queueStart.connect(secondToken)
        firstToken.delete()
        return self.save()

    def list(self):
        if len(self.queueStart) == 0:
            return []
        token = self.queueStart.get()
        result = [token.origin.get()]
        while len(token.nextToken):
            token = token.nextToken.get()
            result.append(token.origin.get())
        return result

    def queueTokenList(self):
        if len(self.queueStart) == 0:
            return []
        token = self.queueStart.get()
        result = [token]
        while len(token.nextToken):
            token = token.nextToken.get()
            result.append(token)
        return result


class KanjiQueue(Queue):
    queueStart = RelationshipTo(
        'KanjiQueueToken', 'QueueStart', cardinality=ZeroOrOne)
    queueEnd = RelationshipTo(
        'KanjiQueueToken', 'QueueEnd', cardinality=ZeroOrOne)

    queueTokens = RelationshipTo('KanjiQueueToken', 'QueueToken')

    def push(self, kanji):
        queueToken = KanjiQueueToken.new(queue=self, origin=kanji)
        super(KanjiQueue, self).push(queueToken)

    def new(**args):
        return KanjiQueue(**args).save()


class WordQueue(Queue):
    queueStart = RelationshipTo(
        'WordQueueToken', 'QueueStart', cardinality=ZeroOrOne)
    queueEnd = RelationshipTo(
        'WordQueueToken', 'QueueEnd', cardinality=ZeroOrOne)

    queueTokens = RelationshipTo('WordQueueToken', 'QueueToken')

    def push(self, word):
        queueToken = WordQueueToken.new(queue=self, origin=word)
        super(WordQueue, self).push(queueToken)

    def new(**args):
        return WordQueue(**args).save()


class UnlearnedKanji(StructuredNode):
    origin = RelationshipTo('Kanji', 'Origin', cardinality=One)
    requirements = RelationshipFrom('QueueToken', 'UnlearnedKanji')
    queue = RelationshipFrom('Queue', 'UnlearnedKanji', cardinality=One)

    @property
    def japanese(self):
        return self.origin.get().japanese

    @property
    def english(self):
        return (self.neo_english if self.neo_english else self.origin.get().english)

    @english.setter
    def english(self, english):
        self.neo_english = english
        return self.save()

    @property
    def furigana(self):
        return self.origin.get().furigana

    @property
    def jlpt(self):
        return self.origin.get().jlpt

    @property
    def kanji(self):
        return self.origin.get().kanji

    def new(origin, **args):
        result = UnlearnedKanji(**args).save()
        result.origin.connect(origin)
        return result.save()

    def check(self):
        if len(self.requirements) == 0:
            self.delete()


class QueueToken(StructuredNode):
    nextToken = RelationshipTo(
        'QueueToken', 'NextQueueToken', cardinality=ZeroOrOne)
    previousToken = RelationshipFrom(
        'QueueToken', 'NextQueueToken', cardinality=ZeroOrOne)
    origin = RelationshipTo('Word', 'Origin', cardinality=One)
    queue = RelationshipFrom('Queue', 'QueueStart', cardinality=ZeroOrOne)
    reqiredUnlearnedKanji = RelationshipTo('UnlearnedKanji', 'UnlearnedKanji')

    def removeNextToken(self):
        if len(self.nextToken):
            self.nextToken.disconnect(self.nextToken.get())
        return self.save()

    def setNextToken(self, nextToken):
        self.removeNextToken()
        self.nextToken.connect(nextToken)
        return self.save()

    def removePreviousToken(self):
        if len(self.previousToken):
            self.previousToken.disconnect(self.previousToken.get())
        return self.save()

    def setPreviousToken(self, previousToken):
        self.removeNextToken()
        self.previousToken.connect(previousToken)
        return self.save()


class KanjiQueueToken(QueueToken):
    nextToken = RelationshipTo(
        'KanjiQueueToken', 'NextQueueToken', cardinality=ZeroOrOne)
    previousToken = RelationshipFrom(
        'KanjiQueueToken', 'NextQueueToken', cardinality=ZeroOrOne)
    origin = RelationshipTo('Kanji', 'Origin', cardinality=One)
    queue = RelationshipFrom('KanjiQueue', 'QueueStart', cardinality=ZeroOrOne)

    @property
    def inscription(self):
        return self.origin.get().inscription

    @property
    def story(self):
        return (self.neo_story if self.neo_story else self.origin.get().story)

    @story.setter
    def story(self, story):
        self.neo_story = story
        return self.save()

    @property
    def meaning(self):
        return (self.neo_meaning if self.neo_meaning else self.origin.get().meaning)

    @meaning.setter
    def meaning(self, meaning):
        self.neo_meaning = meaning
        return self.save()

    @property
    def jlpt(self):
        return self.origin.get().jlpt

    @property
    def frequency(self):
        return self.origin.get().frequency

    @property
    def kunyomi(self):
        return self.origin.get().kunyomi

    @property
    def onyomi(self):
        return self.origin.get().onyomi

    @property
    def jouyou(self):
        return self.origin.get().jouyou

    @property
    def rtk(self):
        return self.origin.get().rtk

    @property
    def radical(self):
        return self.origin.get().radical

    @property
    def primitives(self):
        return self.origin.get().primitives

    def new(queue, origin, **args):
        token = KanjiQueueToken(**args).save()
        token.origin.connect(origin)
        queue.queueTokens.connect(token)
        queue.save()

        primitives = origin.getAllPrimitives()

        for primitive in primitives:
            if not (len(queue.user.get().personalizedKanji.get().filterKanji(inscription=primitive.inscription))
                    or len(queue.filterUnlearnedKanji(inscription=primitive.inscription))):
                UnlearnedKanji.new(primitive)

        for primitive in primitives:
            unlearnedKanji = queue.filterUnlearnedKanji(
                inscription=primitive.inscription)
            if len(unlearnedKanji):
                token.reqiredUnlearnedKanji.connect(unlearnedKanji[0])

        return token.save()

    def pre_delete(self):
        for kanji in self.reqiredUnlearnedKanji.all():
            self.reqiredUnlearnedKanji.disconnect(kanji)
            kanji.check()


class WordQueueToken(QueueToken):
    nextToken = RelationshipTo(
        'WordQueueToken', 'NextQueueToken', cardinality=ZeroOrOne)
    previousToken = RelationshipFrom(
        'WordQueueToken', 'NextQueueToken', cardinality=ZeroOrOne)
    origin = RelationshipTo('Word', 'Origin', cardinality=One)
    queue = RelationshipFrom('WordQueue', 'QueueStart', cardinality=ZeroOrOne)

    @property
    def japanese(self):
        return self.origin.get().japanese

    @property
    def english(self):
        return (self.neo_english if self.neo_english else self.origin.get().english)

    @english.setter
    def english(self, english):
        self.neo_english = english
        return self.save()

    @property
    def furigana(self):
        return self.origin.get().furigana

    @property
    def jlpt(self):
        return self.origin.get().jlpt

    @property
    def kanji(self):
        return self.origin.get().kanji

    def new(queue, origin, **args):
        token = WordQueueToken(**args).save()
        token.origin.connect(origin)
        queue.queueTokens.connect(token)
        queue.save()

        primitives = origin.getAllPrimitives()

        for primitive in primitives:
            if not (len(queue.user.get().personalizedKanji.get().filterKanji(inscription=primitive.inscription))
                    or len(queue.filterUnlearnedKanji(inscription=primitive.inscription))):
                UnlearnedKanji.new(primitive)

        for primitive in primitives:
            unlearnedKanji = queue.filterUnlearnedKanji(
                inscription=primitive.inscription)
            if len(unlearnedKanji):
                token.reqiredUnlearnedKanji.connect(unlearnedKanji[0])

        return token.save()

    def pre_delete(self):
        for kanji in self.reqiredUnlearnedKanji.all():
            self.reqiredUnlearnedKanji.disconnect(kanji)
            kanji.check()

from .user import User
