import inspect


def saves(*arguments):
    """A decorator function used to call .save() on specified
    arguments after the function

    NB: if the function itself raises an error, nothing is saved.
    Also, if some of the arguments can't be saved, those
    specified after it also won't be.

    :param arguments: list[string] - the names of arguments to be savec
    """

    def __try_to_save(parameter, value=None):
        try:
            if value is not None:
                value.save()

            else:
                parameter.default.save()

        except Exception as exception:
            raise (ValueError("Unable to save " + parameter.name))

    def wrap(function):

        def wrapped_function(*args, **kwargs):
            result = function(*args, **kwargs)

            for index, parameter in enumerate(inspect.signature(function).parameters.
                                              values()):
                # argument is not required to be saved
                if parameter.name not in arguments:
                    continue

                # arguments was passed as a keyword argument
                if parameter.name in kwargs.keys():
                    __try_to_save(parameter, kwargs[parameter.name])
                    continue

                # argument was passed as a positional argument
                if index < len(args):
                    __try_to_save(parameter, args[index])
                    continue

                # argument has its default value
                __try_to_save(parameter)

            return result

        return wrapped_function

    return wrap
