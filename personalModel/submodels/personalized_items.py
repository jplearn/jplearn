from neomodel import StructuredNode, RelationshipTo, IntegerProperty, StringProperty, AliasProperty, One, db
from languageModel.models import Kanji, Word


class PersonalizedWordsRoot(StructuredNode):
    words = RelationshipTo('PersonalizedWord', 'PersonalizedWord')

    def new(**args):
        args = {'neo_' + key: value for key, value in args.items()}
        return PersonalizedWordsRoot(**args).save()

    def addPersnalizedWord(self, word):
        self.words.connect(word)
        return self.save()

    def removePersonalizedWord(self, word):
        self.words.disconnect(word)
        return self.save()

    def filterWords(self, **kwargs):
        try:
            return self.words.search(**kwargs)
        except ValueError:

            cypher = "START a=node({self}) MATCH a-[:PersonalizedWord]->(b)-[:BasedOn]->(c)"
            if (len(kwargs)):
                cypher += " WHERE "
            for key, value in kwargs.items():
                cypher += "c.neo_{0} = \'{1}\', ".format(key, value)

            cypher = cypher[0: -2]
            cypher += " RETURN b"

            results, columns = self.cypher(cypher, kwargs)
            return [PersonalizedWord.inflate(row[0]) for row in results]


class PersonalizedKanjiRoot(StructuredNode):
    kanji = RelationshipTo('PersonalizedKanji', 'PersonalizedKanji')

    def new(**args):
        args = {'neo_' + key: value for key, value in args.items()}
        return PersonalizedKanjiRoot(**args).save()

    def addPersonalizedKanji(self, kanji):
        self.kanji.connect(kanji)
        return self.save()

    def removePersonalizedKanji(self, kanji):
        self.kanji.disconnect(kanji)
        return self.save()

    # to rewrite
    def filterKanji(self, **kwargs):
        try:
            return self.kanji.search(**kwargs)
        except ValueError:
            kwargs = {'neo_' + key: value for key, value in kwargs.items()}
            result = []
            for item in self.kanji.all():
                if len(item.origin.search(**kwargs)):
                    result.append(item)
            return result


class PersonalizedItem(StructuredNode):
    neo_jpEngReviewInterval = IntegerProperty(default=0)
    neo_engJpReviewInterval = IntegerProperty(default=0)

    @property
    def jpEngReviewInterval(self):
        return self.neo_jpEngReviewInterval

    @property
    def engJpReviewInterval(self):
        return self.neo_engJpReviewInterval

    @jpEngReviewInterval.setter
    def jpEngReviewInterval(self, interval):
        self.neo_jpEngReviewInterval = interval
        self.save()
        return self

    @engJpReviewInterval.setter
    def engJpReviewInterval(self, interval):
        self.neo_engJpReviewInterval = interval
        self.save()
        return self

    def muliplyJpEngReviewIntervalBy(self, multiplier):
        self.neo_jpEngReviewInterval *= interval
        self.save()
        return self

    def muliplyEngJpReviewIntervalBy(self, multiplier):
        self.neo_engJpReviewInterval *= interval
        self.save()
        return self


class PersonalizedWord(PersonalizedItem):
    origin = RelationshipTo('Word', 'BasedOn', cardinality=One)
    neo_english = StringProperty()

    def new(origin, **args):
        args = {'neo_' + key: value for key, value in args.items()}
        result = PersonalizedWord(**args).save()
        result.origin.connect(origin)
        return result.save()

    @property
    def japanese(self):
        return self.origin.get().japanese

    @property
    def english(self):
        return (self.neo_english if self.neo_english else self.origin.get().english)

    @english.setter
    def english(self, english):
        self.neo_english = english
        return self.save()

    @property
    def furigana(self):
        return self.origin.get().furigana

    @property
    def jlpt(self):
        return self.origin.get().jlpt

    @property
    def kanji(self):
        return self.origin.get().kanji


class PersonalizedKanji(PersonalizedItem):
    origin = RelationshipTo('Kanji', 'BasedOn', cardinality=One)
    neo_story = StringProperty()
    neo_meaning = StringProperty()

    @property
    def inscription(self):
        return self.origin.get().inscription

    @property
    def story(self):
        return (self.neo_story if self.neo_story else self.origin.get().story)

    @story.setter
    def story(self, story):
        self.neo_story = story
        return self.save()

    @property
    def meaning(self):
        return (self.neo_meaning if self.neo_meaning else self.origin.get().meaning)

    @meaning.setter
    def meaning(self, meaning):
        self.neo_meaning = meaning
        return self.save()

    @property
    def jlpt(self):
        return self.origin.get().jlpt

    @property
    def frequency(self):
        return self.origin.get().frequency

    @property
    def kunyomi(self):
        return self.origin.get().kunyomi

    @property
    def onyomi(self):
        return self.origin.get().onyomi

    @property
    def jouyou(self):
        return self.origin.get().jouyou

    @property
    def rtk(self):
        return self.origin.get().rtk

    @property
    def radical(self):
        return self.origin.get().radical

    @property
    def primitives(self):
        return self.origin.get().primitives

    def new(origin, **args):
        args = {'neo_' + key: value for key, value in args.items()}
        result = PersonalizedKanji(**args).save()
        result.origin.connect(origin)
        return result.save()
