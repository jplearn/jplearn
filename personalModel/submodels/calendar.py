import random
import functools
from datetime import date as ddate, timedelta

from django.db import models

from neomodel import (StructuredNode, DateProperty, IntegerProperty, AliasProperty,
                      RelationshipTo, RelationshipFrom, ZeroOrOne, One, db)

from .personalized_items import PersonalizedItem


class Calendar(StructuredNode):
    calendarStart = RelationshipTo(
        'CalendarNode', 'CalendarStart', cardinality=ZeroOrOne)
    nextUnrevisedDay = RelationshipTo(
        'FutureCalendarNode', 'NextUnrevisedDay', cardinality=ZeroOrOne)
    lastRevisedDay = RelationshipTo(
        'PastCalendarNode', 'LastRevisedDay', cardinality=ZeroOrOne)
    futureDays = RelationshipTo(
        'FutureCalendarNode', 'Day')
    pastDays = RelationshipTo(
        'PastCalendarNode', 'Day')

    def hasNextItem(self):
        return len(self.nextUnrevisedDay) and self.nextUnrevisedDay.get().date <= ddate.today()

    def getNextItem(self):
        if not self.hasNextItem():
            return None
        elif self.nextUnrevisedDay.get().date > ddate.today():
            return None
        else:
            return self.nextUnrevisedDay.get().getRandomItemToRevise()

    def setNextUnrevisedDay(self, day):
        self.removeNextUnrevisedDay()
        self.nextUnrevisedDay.connect(day)
        return self.save()

    def removeNextUnrevisedDay(self):
        if len(self.nextUnrevisedDay):
            self.nextUnrevisedDay.disconnect(self.nextUnrevisedDay.get())
        return self.save()

    def setLastRevisedDay(self, day):
        self.removeLastRevisedDay()
        self.lastRevisedDay.connect(day)
        return self.save()

    def removeLastRevisedDay(self):
        if len(self.lastRevisedDay):
            self.lastRevisedDay.disconnect(self.lastRevisedDay.get())
        return self.save()

    def insertDayWithItem(self, date, item):
        newDay = FutureCalendarNode.new(
            date=date, item=item, calendar=self).save()
        self.futureDays.connect(newDay)

        if len(self.nextUnrevisedDay) == 0:
            self.setNextUnrevisedDay(newDay)
            return self.save()
        elif self.nextUnrevisedDay.get().date > date:
            newDay.nextDay.connect(self.nextUnrevisedDay.get())
            self.setNextUnrevisedDay(newDay)
            newDay.save()
            return self.save()
        else:
            currentDay = self.nextUnrevisedDay.get()

            while (len(currentDay.nextDay)) and (currentDay.nextDay.get().date < date):
                currentDay = currentDay.nextDay.get()

            if len(currentDay.nextDay):
                newDay.setNextDay(currentDay.nextDay.get())
            currentDay.setNextDay(newDay)

            newDay.save()
            currentDay.save()
            return self.save()

    def insertItemAtDate(self, date, item):
        if self.futureDays.search(date=date):
            self.futureDays.search(date=date)[0].addItem(item)
        else:
            self.insertDayWithItem(date=date, item=item)

    def insertItemToday(self, item):
        return self.insertItemAtDate(date=ddate.today(), item=item)

    def removeItem(self, item):
        self.nextUnrevisedDay.get().removeItem(item)

    def insertTodayStatistic(self):
        today = PastCalendarNode.new(calendar=self)
        self.pastDays.connect(today)
        if len(self.lastRevisedDay):
            self.lastRevisedDay.get().setNextDay(today)
        self.setLastRevisedDay(today)
        return self.save()

    def todayStatistic(func):
        def func_wrapper(self, *args, **kwargs):
            date = self.pastDays.search(date=ddate.today())
            if date == []:
                self.insertTodayStatistic()
            func(self, *args, **kwargs)
        return func_wrapper

    @todayStatistic
    def addRevisedCorrectly(self):
        self.pastDays.search(date=ddate.today())[0].addRevisedCorrectly()
        return self

    @todayStatistic
    def addRevised(self):
        self.pastDays.search(date=ddate.today())[0].addRevised()
        return self

    @todayStatistic
    def addLearned(self):
        self.pastDays.search(date=ddate.today())[0].addLearned()
        return self

    def new(**args):
        return Calendar(**args).save()


class KanjiCalendar(Calendar):

    def new(**args):
        return KanjiCalendar(**args).save()


class WordsCalendar(Calendar):

    def new(**args):
        return WordsCalendar(**args).save()


class CalendarNode(StructuredNode):
    date = DateProperty(default=lambda: ddate.today())

    nextDay = RelationshipTo('CalendarNode', 'NextDay', cardinality=ZeroOrOne)
    calendar = RelationshipFrom('Calendar', 'Day', cardinality=One)

    def setNextDay(self, day):
        self.removeNextDay()
        self.nextDay.connect(day)
        return self.save()

    def removeNextDay(self):
        if len(self.nextDay):
            self.nextDay.disconnect(self.nextDay.get())
        return self.save()


class PastCalendarNode(CalendarNode):
    revised = IntegerProperty(default=0)
    learned = IntegerProperty(default=0)
    revisedCorrectly = IntegerProperty(default=0)

    def addRevisedCorrectly(self):
        self.revisedCorrectly += 1
        return self.addRevised().save()

    def addRevised(self):
        self.revised += 1
        return self.save()

    def addLearned(self):
        self.learned += 1
        return self.save()

    def new(calendar, **args):
        day = PastCalendarNode(**args).save()
        calendar.pastDays.connect(day)
        calendar.save()
        return day.save()


class FutureCalendarNode(CalendarNode):
    itemsToRevise = RelationshipTo('PersonalizedItem', 'toRevise')

    def getRandomItemToRevise(self):
        return random.choice(self.itemsToRevise.all())

    def removeItem(self, item):
        self.itemsToRevise.disconnect(item)

        if len(self.itemsToRevise) == 0:
            self.calendar.nextUnrevisedDay = self.nextDay
            self.calendar.get().save()
            self.delete()
        else:
            return self.save()

    def addItem(self, item):
        self.itemsToRevise.connect(item)
        return self.save()

    def new(calendar, item, **args):
        day = FutureCalendarNode(**args).save()
        day.itemsToRevise.connect(item)
        calendar.futureDays.connect(day)
        calendar.save()
        return day.save()
