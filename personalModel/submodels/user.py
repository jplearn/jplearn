from django.db import models

from neomodel import (StructuredNode, StringProperty, BooleanProperty, ArrayProperty, DateTimeProperty,
                      IntegerProperty, RelationshipTo, RelationshipFrom, ZeroOrOne, One, db)

from languageModel.models import Kanji, Radical, Word
from .personalized_items import PersonalizedKanjiRoot, PersonalizedWordsRoot
from .calendar import KanjiCalendar, WordsCalendar


class User(StructuredNode):
    username = StringProperty(required=True)
    password = StringProperty(required=True)

    settings = RelationshipTo('Settings', 'Settings', cardinality=One)
    personalInfo = RelationshipTo(
        'PersonalInfo', 'PersonalInfo', cardinality=One)
    kanjiQueue = RelationshipTo(
        'KanjiQueue', 'Queue', cardinality=One)
    wordQueue = RelationshipTo(
        'WordQueue', 'Queue', cardinality=One)
    personalizedKanji = RelationshipTo(
        'PersonalizedKanjiRoot', 'PersonalizedKanjiRoot', cardinality=One)
    personalizedWords = RelationshipTo(
        'PersonalizedWordsRoot', 'PersonalizedWordsRoot', cardinality=One)

    kanjiCalendar = RelationshipTo(
        'KanjiCalendar', 'Calendar', cardinality=ZeroOrOne)

    wordsCalendar = RelationshipTo(
        'WordsCalendar', 'Calendar', cardinality=ZeroOrOne)

    def new(**args):
        user = User(**args).save()
        user.settings.connect(Settings.new())
        user.personalInfo.connect(PersonalInfo.new())
        user.wordQueue.connect(WordQueue.new())
        user.kanjiQueue.connect(KanjiQueue.new())
        user.personalizedKanji.connect(PersonalizedKanjiRoot.new())
        user.personalizedWords.connect(PersonalizedWordsRoot.new())
        user.kanjiCalendar.connect(KanjiCalendar.new())
        user.wordsCalendar.connect(WordsCalendar.new())
        return user.save()


class Settings(StructuredNode):
    numberOfKanjiToLearn = IntegerProperty(default=10)
    numberOfWordsToLearn = IntegerProperty(default=10)
    jpToEngKanji = BooleanProperty(default=True)
    engToJpKanji = BooleanProperty(default=True)
    jpToEngWords = BooleanProperty(default=True)
    engToJpWords = BooleanProperty(default=True)

    def new(**args):
        return Settings(**args).save()


class PersonalInfo(StructuredNode):
    firstName = StringProperty()
    secondName = StringProperty()
    email = StringProperty()

    def new(**args):
        return PersonalInfo(**args).save()

from .queue import KanjiQueue, WordQueue
