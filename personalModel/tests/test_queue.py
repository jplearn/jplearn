import sys
sys.path.append('../..')

from datetime import date
from random import shuffle
from django.test import TestCase
from personalModel.submodels.user import *
from personalModel.submodels.queue import *
from languageModel.models import *
from test import Neo4jTestCase
from datetime import timedelta
from personalModel.submodels.personalized_items import PersonalizedKanji


class KanjiQueueTest(Neo4jTestCase):

    def setUp(self):
        self.user = User.new(username='test', password='test')
        self.learnedKanji = [Kanji.new(inscription=str(i) + ' learned', meaning=str(i) + ' learned')
                             for i in range(4)]

        self.unlearnedKanji = [Kanji.new(inscription=str(i) + ' unlearned', meaning=str(i) + ' unlearned')
                               for i in range(4)]
        for idx, unlearnedKanji in enumerate(self.unlearnedKanji):
            unlearnedKanji.addPrimitive(self.learnedKanji[idx])
            if idx < 3:
                unlearnedKanji.addPrimitive(self.unlearnedKanji[idx + 1])
        for kanji in self.learnedKanji:
            self.user.personalizedKanji.get().addPersonalizedKanji(PersonalizedKanji.new(kanji))

        self.kanjiQueue = self.user.kanjiQueue.get()

    def testPushEmpty(self):
        self.kanjiQueue.push(self.unlearnedKanji[0])
        self.assertEqual(self.kanjiQueue.front(), self.unlearnedKanji[0])

    def testPushMultiple(self):
        for kanji in self.unlearnedKanji:
            self.kanjiQueue.push(kanji)
            self.assertEqual(self.kanjiQueue.back(), kanji)
        self.assertEqual(self.kanjiQueue.list(), self.unlearnedKanji)

    def testPopBack(self):
        for kanji in self.unlearnedKanji:
            self.kanjiQueue.push(kanji)
            self.assertEqual(self.kanjiQueue.back(), kanji)
        for idx in range(len(self.unlearnedKanji) - 1, -1, -1):
            self.kanjiQueue.popBack()
            self.assertEqual(self.kanjiQueue.list(),
                             self.unlearnedKanji[:idx])

    def testPopFront(self):
        for kanji in self.unlearnedKanji:
            self.kanjiQueue.push(kanji)
        for idx in range(len(self.unlearnedKanji)):
            self.kanjiQueue.popFront()
            self.assertEqual(self.kanjiQueue.list(),
                             self.unlearnedKanji[idx + 1:])

    def testQueueTokenList(self):
        for kanji in self.unlearnedKanji:
            self.kanjiQueue.push(kanji)
        for idx, token in enumerate(self.kanjiQueue.queueTokenList()):
            self.assertEqual(token.origin.get(), self.unlearnedKanji[idx])


class WordQueueTest(Neo4jTestCase):

    def setUp(self):
        self.user = User.new(username='test', password='test')
        self.learnedKanji = [Kanji.new(inscription=str(i) + ' learned', meaning=str(i) + ' learned')
                             for i in range(4)]

        self.unlearnedKanji = [Kanji.new(inscription=str(i) + ' unlearned', meaning=str(i) + ' unlearned')
                               for i in range(4)]

        self.words = [Word.new(english=str(i) + ' english',
                               japanese=str(i) + ' japanese') for i in range(4)]
        for idx, word in enumerate(self.words):
            word.addKanji(self.learnedKanji[idx])
            word.addKanji(self.unlearnedKanji[idx])
        for kanji in self.learnedKanji:
            self.user.personalizedKanji.get().addPersonalizedKanji(PersonalizedKanji.new(kanji))

        self.wordQueue = self.user.wordQueue.get()

    def testPushEmpty(self):
        self.wordQueue.push(self.words[0])
        self.assertEqual(self.wordQueue.front(), self.words[0])

    def testPushMultiple(self):
        for word in self.words:
            self.wordQueue.push(word)
            self.assertEqual(self.wordQueue.back(), word)
        self.assertEqual(self.wordQueue.list(), self.words)

    def testPopBack(self):
        for word in self.words:
            self.wordQueue.push(word)
            self.assertEqual(self.wordQueue.back(), word)
        for idx in range(len(self.words) - 1, -1, -1):
            self.wordQueue.popBack()
            self.assertEqual(self.wordQueue.list(),
                             self.words[:idx])

    def testPopFront(self):
        for word in self.words:
            self.wordQueue.push(word)
        for idx in range(len(self.words)):
            self.wordQueue.popFront()
            self.assertEqual(self.wordQueue.list(),
                             self.words[idx + 1:])

    def testQueueTokenList(self):
        for word in self.words:
            self.wordQueue.push(word)
        for idx, token in enumerate(self.wordQueue.queueTokenList()):
            self.assertEqual(token.origin.get(), self.words[idx])
