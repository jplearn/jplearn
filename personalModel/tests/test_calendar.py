import sys
sys.path.append('../..')

from datetime import date
from random import shuffle
from django.test import TestCase
from personalModel.submodels.calendar import *
from personalModel.submodels.personalized_items import *
from languageModel.models import *
from test import Neo4jTestCase
from datetime import timedelta


class CalendarTest(Neo4jTestCase):

    def setUp(self):
        self.calendar = Calendar.new()
        self.kanji = [Kanji.new(inscription=str(i), meaning=str(i))
                      for i in range(4)]
        self.personalizedKanji = [
            PersonalizedKanji.new(kanji) for kanji in self.kanji]

    def testHasNextItem(self):
        self.assertEqual(self.calendar.hasNextItem(), False)

    def testInsertItemAtDate(self):
        self.calendar.insertItemAtDate(
            date.today() + timedelta(1), self.personalizedKanji[0])
        self.assertEqual(self.calendar.getNextItem(), None)

        days = self.calendar.futureDays.search(
            date=date.today() + timedelta(1))
        self.assertEqual(len(days), 1)
        day = days[0]

        self.assertEqual(len(day.itemsToRevise.all()), 1)
        self.assertEqual(day.itemsToRevise.all()[0], self.personalizedKanji[0])

    def testInsertTodayItem(self):
        self.calendar.insertItemToday(self.personalizedKanji[0])
        self.assertEqual(self.calendar.getNextItem(), self.personalizedKanji[0])

    def testInsertTodayItemMultiple(self):
        for personalizedKanji in self.personalizedKanji:
            self.calendar.insertItemToday(personalizedKanji)
        self.assertIn(self.calendar.getNextItem(), self.personalizedKanji)

    def testInsertItemAtDateMultiple(self):
        perm = (2, 0, 3, 1)
        for idx in perm:
            self.calendar.insertItemAtDate(
                date=date.today() + timedelta(idx), item=self.personalizedKanji[idx])

        for idx in perm:
            days = self.calendar.futureDays.search(
                date=date.today() + timedelta(idx))
            self.assertEqual(len(days), 1)
            day = days[0]
            self.assertEqual(len(day.itemsToRevise.all()), 1)
            self.assertEqual(
                day.itemsToRevise.all()[0], self.personalizedKanji[idx])

        day = self.calendar.nextUnrevisedDay.get()
        self.assertEqual(day.date, date.today())
        for idx in range(1, 4):
            day = day.nextDay.get()
            self.assertEqual(day.date, date.today() + timedelta(idx))

    def testAutoremovalOfFutureDays(self):
        self.calendar.insertItemToday(self.personalizedKanji[0])
        self.assertEqual(self.calendar.getNextItem(), self.personalizedKanji[0])
        self.calendar.removeItem(self.personalizedKanji[0])
        self.calendar.refresh()
        self.assertEqual(self.calendar.getNextItem(), None)

    def testStatistics(self):
        revised = 5
        revisedCorrectly = 11
        learned = 4

        toDo = [self.calendar.addRevised] * revised
        toDo += [self.calendar.addRevisedCorrectly] * revisedCorrectly
        toDo += [self.calendar.addLearned] * learned

        shuffle(toDo)
        for f in toDo:
            f()

        self.assertEqual(
            self.calendar.lastRevisedDay.get().revisedCorrectly, revisedCorrectly)
        self.assertEqual(
            self.calendar.lastRevisedDay.get().revised, revised + revisedCorrectly)
        self.assertEqual(self.calendar.lastRevisedDay.get().learned, learned)
