import sys
sys.path.append('../..')

from datetime import date
from django.test import TestCase
from datetime import timedelta

from test import Neo4jTestCase
from personalModel.submodels.personalized_items import PersonalizedKanji, PersonalizedWord, PersonalizedKanjiRoot, PersonalizedWordsRoot
from languageModel.models import Kanji, Word


class PersonalizedKanjiCreateTest(Neo4jTestCase):

    def setUp(self):
        self.kanji = Kanji.new(inscription='円', meaning='yen')
        self.kanji.refresh()

    def testPersonalizedKanjiCreation(self):
        self.personalizedKanji = PersonalizedKanji.new(origin=self.kanji)
        self.assertEqual(self.personalizedKanji.inscription, '円')
        self.assertEqual(self.personalizedKanji.meaning, 'yen')


class PersonalizedKanjiTest(Neo4jTestCase):

    def setUp(self):
        self.kanji = Kanji.new(
            inscription='円', meaning='yen', story='story', jlpt='jlpt', frequency=1)
        self.kanji.refresh()

    def testSetters(self):
        self.personalizedKanji = PersonalizedKanji.new(origin=self.kanji)
        self.personalizedKanji.refresh()

        self.assertEqual(self.personalizedKanji.story, 'story')
        self.personalizedKanji.story = 'new story'
        self.personalizedKanji.refresh()
        self.assertEqual(self.personalizedKanji.story, 'new story')

        self.assertEqual(self.personalizedKanji.meaning, 'yen')
        self.personalizedKanji.meaning = 'new meaning'
        self.personalizedKanji.refresh()
        self.assertEqual(self.personalizedKanji.meaning, 'new meaning')

        self.personalizedKanji.jpEngReviewInterval = 10
        self.personalizedKanji.engJpReviewInterval = 11
        self.personalizedKanji.refresh()
        self.assertEqual(self.personalizedKanji.engJpReviewInterval, 11)
        self.assertEqual(self.personalizedKanji.jpEngReviewInterval, 10)

    def testGetters(self):
        self.personalizedKanji = PersonalizedKanji.new(origin=self.kanji)
        self.personalizedKanji.refresh()

        self.assertEqual(self.personalizedKanji.inscription, '円')
        self.assertEqual(self.personalizedKanji.meaning, 'yen')
        self.assertEqual(self.personalizedKanji.story, 'story')
        self.assertEqual(self.personalizedKanji.jlpt, 'jlpt')
        self.assertEqual(self.personalizedKanji.frequency, 1)
        self.assertEqual(self.personalizedKanji.kunyomi, None)
        self.assertEqual(self.personalizedKanji.onyomi, None)
        self.assertEqual(self.personalizedKanji.jouyou, None)
        self.assertEqual(self.personalizedKanji.rtk, None)


class PersonalizedWordCreateTest(Neo4jTestCase):

    def setUp(self):
        self.word = Word.new(
            english='english', japanese='japanese', furigana='furigana')

    def testPersonalizedWordCreation(self):
        self.personalizedWord = PersonalizedWord.new(origin=self.word)
        self.assertEqual(self.personalizedWord.english, 'english')


class PersonalizedWordsTest(Neo4jTestCase):

    def setUp(self):
        self.word = Word.new(
            english='english', japanese='japanese', furigana='furigana', jlpt='jlpt')
        self.word.refresh()

    def testSetters(self):
        self.personalizedWord = PersonalizedWord.new(origin=self.word)

        self.assertEqual(self.personalizedWord.english, 'english')
        self.personalizedWord.english = 'new english'
        self.personalizedWord.refresh()
        self.assertEqual(self.personalizedWord.english, 'new english')

        self.personalizedWord.jpEngReviewInterval = 10
        self.personalizedWord.engJpReviewInterval = 11
        self.personalizedWord.refresh()
        self.assertEqual(self.personalizedWord.engJpReviewInterval, 11)
        self.assertEqual(self.personalizedWord.jpEngReviewInterval, 10)

    def testGetters(self):
        self.personalizedWord = PersonalizedWord.new(origin=self.word)
        self.personalizedWord.refresh()

        self.assertEqual(self.personalizedWord.english, 'english')
        self.assertEqual(self.personalizedWord.japanese, 'japanese')
        self.assertEqual(self.personalizedWord.furigana, 'furigana')
        self.assertEqual(self.personalizedWord.jlpt, 'jlpt')


class PersonalizedKanjiRootCreateTest(TestCase):

    def testCreation(self):
        self.kanjiRoot = PersonalizedKanjiRoot.new()


class PersonalizedKanjiRootTest(TestCase):

    def setUp(self):
        self.kanji = Kanji.new(
            inscription='円', meaning='yen', story='story', jlpt='jlpt', frequency=1)
        self.personalizedKanji = PersonalizedKanji.new(self.kanji)
        self.kanjiRoot = PersonalizedKanjiRoot.new()

    def testAdd(self):
        self.kanjiRoot.addPersonalizedKanji(self.personalizedKanji)
        self.kanjiRoot.refresh()
        for kanji in self.kanjiRoot.kanji:
            self.assertEqual(kanji.inscription, '円')

    def testFilterKanji(self):
        self.kanjiRoot.addPersonalizedKanji(self.personalizedKanji)

        self.assertEqual(self.kanjiRoot.filterKanji(
            inscription='円'), [self.personalizedKanji])

        self.assertEqual(self.kanjiRoot.filterKanji(
            inscription='q'), [])

    def testAddSame(self):
        self.kanjiRoot.addPersonalizedKanji(self.personalizedKanji)
        self.kanjiRoot.addPersonalizedKanji(self.personalizedKanji)
        self.kanjiRoot.removePersonalizedKanji(self.personalizedKanji)
        self.assertEqual(len(self.kanjiRoot.kanji), 0)


class PersonalizedWordsRootTest(TestCase):

    def setUp(self):
        self.word = Word.new(
            english='english', japanese='japanese', furigana='furigana')
        self.personalizedWord = PersonalizedWord.new(self.word)
        self.wordRoot = PersonalizedWordsRoot.new()

    def testAdd(self):
        self.wordRoot.addPersnalizedWord(self.personalizedWord)
        self.wordRoot.refresh()
        for word in self.wordRoot.words:
            self.assertEqual(word.english, 'english')

    def testFilterWords(self):
        self.wordRoot.addPersnalizedWord(self.personalizedWord)

        self.assertEqual(self.wordRoot.filterWords(
            english='english'), [self.personalizedWord])

        self.assertEqual(self.wordRoot.filterWords(
            english='q'), [])

    def testAddSame(self):
        self.wordRoot.addPersnalizedWord(self.personalizedWord)
        self.wordRoot.addPersnalizedWord(self.personalizedWord)
        self.wordRoot.removePersonalizedWord(self.personalizedWord)
        self.assertEqual(len(self.wordRoot.words), 0)
