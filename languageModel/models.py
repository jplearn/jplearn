from neomodel import (StructuredNode, StringProperty,
                      IntegerProperty, RelationshipTo, RelationshipFrom, ZeroOrOne)


class baseJpnCharacter(StructuredNode):
    neo_inscription = StringProperty(required=True)
    neo_meaning = StringProperty(required=True)

    @property
    def inscription(self):
        return self.neo_inscription

    @inscription.setter
    def inscription(self, inscription):
        self.neo_inscription = inscription
        self.save()
        return self

    @property
    def meaning(self):
        return self.neo_meaning

    @meaning.setter
    def meaning(self, meaning):
        self.neo_meaning = meaning
        self.save()
        return self


class Kanji(baseJpnCharacter):
    neo_story = StringProperty()
    neo_jlpt = StringProperty()
    neo_frequency = IntegerProperty()

    neo_kunyomi = StringProperty()
    neo_onyomi = StringProperty()
    neo_jouyou = StringProperty()
    neo_rtk = IntegerProperty()

    primitives = RelationshipTo('Kanji', 'ConsistsFrom')
    radical = RelationshipTo('Radical', 'OriginatesFrom', cardinality=ZeroOrOne)

    def new(**args):
        args = {'neo_' + key: value for key, value in args.items()}
        return Kanji(**args).save()

    def addPrimitive(self, primitive):
        self.primitives.connect(primitive)
        return self.save()

    def removePrimitive(self, primitive):
        self.primitives.disconnect(primitive)
        return self.save()

    def setRadical(self, radical):
        self.removeRadical()
        self.radical.connect(radical)
        return self.save()

    def removeRadical(self):
        if len(self.radical):
            self.radical.disconnect(self.radical.get())
        return self.save()

    def getAllPrimitives(self):
        cypher = "START a=node({self}) MATCH a-[:ConsistsFrom*]->(b)"
        cypher += " RETURN b"

        results, columns = self.cypher(cypher)
        return [Kanji.inflate(row[0]) for row in results]

    @property
    def story(self):
        return self.neo_story

    @story.setter
    def story(self, story):
        self.neo_story = story
        self.save()
        return self

    @property
    def jlpt(self):
        return self.neo_jlpt

    @jlpt.setter
    def jlpt(self, jlpt):
        self.neo_jlpt = jlpt
        self.save()
        return self

    @property
    def frequency(self):
        return self.neo_frequency

    @frequency.setter
    def frequency(self, frequency):
        self.neo_frequency = frequency
        self.save()
        return self

    @property
    def kunyomi(self):
        return self.neo_kunyomi

    @kunyomi.setter
    def kunyomi(self, kunyomi):
        self.neo_kunyomi = kunyomi
        self.save()
        return self

    @property
    def onyomi(self):
        return self.neo_onyomi

    @onyomi.setter
    def onyomi(self, onyomi):
        self.neo_onyomi = onyomi
        self.save()
        return self

    @property
    def rtk(self):
        return self.neo_rtk

    @rtk.setter
    def rtk(self, rtk):
        self.neo_rtk = rtk
        self.save()
        return self

    @property
    def jouyou(self):
        return self.neo_jouyou

    @jouyou.setter
    def jouyou(self, jouyou):
        self.neo_jouyou = jouyou
        self.save()
        return self


class Radical(baseJpnCharacter):

    def new(**args):
        args = {'neo_' + key: value for key, value in args.items()}
        result = Radical(**args)
        return result.save()


class Word(StructuredNode):
    neo_english = StringProperty()
    neo_japanese = StringProperty()
    neo_furigana = StringProperty()
    neo_jlpt = StringProperty()

    kanji = RelationshipTo('Kanji', 'Contains')

    def new(**args):
        args = {'neo_' + key: value for key, value in args.items()}
        return Word(**args).save()

    @property
    def english(self):
        return self.neo_english

    @english.setter
    def english(self, english):
        self.neo_english = english
        self.save()
        return self

    @property
    def japanese(self):
        return self.neo_japanese

    @japanese.setter
    def japanese(self, japanese):
        self.neo_japanese = japanese
        self.save()
        return self

    @property
    def furigana(self):
        return self.neo_furigana

    @furigana.setter
    def furigana(self, furigana):
        self.neo_furigana = furigana
        self.save()
        return self

    @property
    def jlpt(self):
        return self.neo_jlpt

    @jlpt.setter
    def jlpt(self, jlpt):
        self.neo_jlpt = jlpt
        self.save()
        return self

    def addKanji(self, kanji):
        self.kanji.connect(kanji)
        return self.save()

    def removeKanji(self, kanji):
        self.kanji.disconnect(kanji)
        return self.save()

    def getAllPrimitives(self):
        cypher = "START a=node({self}) MATCH a-[:Contains]->()-[:ConsistsFrom*]->(b)"
        cypher += " RETURN b"
        cypher += " UNION START a=node({self}) MATCH a-[:Contains]->(b)"
        cypher += " RETURN b"

        results, columns = self.cypher(cypher)
        return [Kanji.inflate(row[0]) for row in results]
