from django.test import TestCase
from .models import Kanji, Radical, Word

import sys
sys.path.append('../..')

from test import Neo4jTestCase


class RadicalCreateTest(Neo4jTestCase):

    def testRadicalCreation(self):
        self.radical = Radical.new(inscription='円', meaning='yen')
        self.radical.refresh()
        self.assertEqual(self.radical.meaning, 'yen')
        self.assertEqual(self.radical.inscription, '円')


class RadicalTest(Neo4jTestCase):

    def setUp(self):
        self.radical = Radical.new(inscription='円', meaning='yen')

    def testSetInscription(self):
        self.radical.inscription = '人'
        self.radical.refresh()
        self.assertEqual(self.radical.inscription, '人')

    def testSetMeaning(self):
        self.radical.meaning = 'person'
        self.radical.refresh()
        self.assertEqual(self.radical.meaning, 'person')


class KanjiCreateTest(Neo4jTestCase):

    def testKanjiCreation(self):
        self.kanji = Kanji.new(
            inscription='円', meaning='yen', story='story', jlpt='jlpt', frequency=1)
        self.kanji.refresh()
        self.assertEqual(self.kanji.meaning, 'yen')
        self.assertEqual(self.kanji.inscription, '円')
        self.assertEqual(self.kanji.story, 'story')
        self.assertEqual(self.kanji.jlpt, 'jlpt')
        self.assertEqual(self.kanji.frequency, 1)


class KanjiSettersTest(Neo4jTestCase):

    def setUp(self):
        self.kanji = Kanji.new(
            inscription='円', meaning='yen', story='story', frequency=1)

    def testSetters(self):
        self.kanji.inscription = '人'
        self.kanji.meaning = 'person'
        self.kanji.story = 'new story'
        self.kanji.jlpt = 'new jlpt'
        self.kanji.frequency = 2
        self.kanji.kunyomi = 'kunyomi'
        self.kanji.onyomi = 'onyomi'
        self.kanji.jouyou = 'jouyou'
        self.kanji.rtk = 1

        self.kanji.refresh()
        self.assertEqual(self.kanji.inscription, '人')
        self.assertEqual(self.kanji.meaning, 'person')
        self.assertEqual(self.kanji.story, 'new story')
        self.assertEqual(self.kanji.jlpt, 'new jlpt')
        self.assertEqual(self.kanji.frequency, 2)
        self.assertEqual(self.kanji.kunyomi, 'kunyomi')
        self.assertEqual(self.kanji.onyomi, 'onyomi')
        self.assertEqual(self.kanji.jouyou, 'jouyou')
        self.assertEqual(self.kanji.rtk, 1)


class KanjiRelationsTest(Neo4jTestCase):

    def setUp(self):
        self.kanji = Kanji.new(inscription='円', meaning='yen')
        self.primitives = [Kanji.new(inscription='a', meaning='aa'),
                           Kanji.new(inscription='b', meaning='bb')]
        self.radicals = [Radical.new(inscription='c', meaning='cc'),
                         Radical.new(inscription='d', meaning='dd')]

    def testAddPrimitiveSingle(self):
        self.kanji.addPrimitive(self.primitives[0])
        for primitive in self.kanji.primitives:
            self.assertEqual(primitive.inscription, 'a')

    def testAddPrimitiveMultiple(self):
        self.kanji.addPrimitive(self.primitives[0])
        self.kanji.addPrimitive(self.primitives[1])

        self.assertEqual(len(self.kanji.primitives), 2)
        self.assertNotEqual(self.kanji.primitives[0], self.kanji.primitives[1])
        for primitive in self.kanji.primitives:
            self.assertIn(primitive.inscription, ['a', 'b'])

    def testRemovePrimitive(self):
        self.kanji.addPrimitive(self.primitives[0])
        self.kanji.removePrimitive(self.primitives[0])
        self.assertEqual(len(self.kanji.primitives), 0)

        self.kanji.addPrimitive(self.primitives[0])
        self.kanji.addPrimitive(self.primitives[1])
        self.kanji.removePrimitive(self.primitives[0])
        self.assertEqual(len(self.kanji.primitives), 1)
        self.kanji.removePrimitive(self.primitives[1])
        self.assertEqual(len(self.kanji.primitives), 0)

    def testSetRadical(self):
        self.kanji.setRadical(self.radicals[0])
        self.assertEqual(self.kanji.radical.get().inscription, 'c')

    def testRemoveRadical(self):
        self.kanji.setRadical(self.radicals[0])
        self.assertEqual(self.kanji.radical.get().inscription, 'c')

    def testSetRadicalAnother(self):
        self.kanji.setRadical(self.radicals[0])
        self.kanji.setRadical(self.radicals[1])
        self.assertEqual(self.kanji.radical.get().inscription, 'd')

    def testGetAllPrimitives(self):
        self.primitives[0].addPrimitive(self.primitives[1])
        self.kanji.addPrimitive(self.primitives[0])
        self.assertEqual(self.kanji.getAllPrimitives(), self.primitives)


class WordCreateTest(Neo4jTestCase):

    def testWordCreation(self):
        self.word = Word.new(
            english='english', japanese='japanese', furigana='furigana', jlpt='jlpt')
        self.word.refresh()
        self.assertEqual(self.word.english, 'english')
        self.assertEqual(self.word.japanese, 'japanese')
        self.assertEqual(self.word.furigana, 'furigana')
        self.assertEqual(self.word.jlpt, 'jlpt')


class WordSettersTest(Neo4jTestCase):

    def setUp(self):
        self.word = Word.new(
            english='english', japanese='japanese', furigana='furigana', jlpt='jlpt')

    def testSetEnglish(self):
        self.word.english = 'new english'
        self.word.japanese = 'new japanese'
        self.word.furigana = 'new furigana'
        self.word.jlpt = 'new jlpt'
        self.word.refresh()
        self.assertEqual(self.word.english, 'new english')
        self.assertEqual(self.word.japanese, 'new japanese')
        self.assertEqual(self.word.furigana, 'new furigana')
        self.assertEqual(self.word.jlpt, 'new jlpt')


class WordRelationsTest(Neo4jTestCase):

    def setUp(self):
        self.word = Word.new(
            english='english', japanese='japanese', furigana='furigana')

        self.kanji = [Kanji.new(inscription='a', meaning='aa'),
                      Kanji.new(inscription='b', meaning='bb')]

    def testAddKanjiSingle(self):
        self.word.addKanji(self.kanji[0])
        for kanji in self.word.kanji:
            self.assertEqual(kanji.inscription, 'a')

    def testAddKanjiMultiple(self):
        self.word.addKanji(self.kanji[0])
        self.word.addKanji(self.kanji[1])

        self.assertEqual(len(self.word.kanji), 2)
        self.assertNotEqual(self.word.kanji[0], self.word.kanji[1])
        for kanji in self.word.kanji:
            self.assertIn(kanji.inscription, ['a', 'b'])

    def testRemoveKanji(self):
        self.word.addKanji(self.kanji[0])
        self.word.removeKanji(self.kanji[0])
        self.assertEqual(len(self.word.kanji), 0)

        self.word.addKanji(self.kanji[0])
        self.word.addKanji(self.kanji[1])
        self.word.removeKanji(self.kanji[0])
        self.assertEqual(len(self.word.kanji), 1)
        self.word.removeKanji(self.kanji[1])
        self.assertEqual(len(self.word.kanji), 0)
